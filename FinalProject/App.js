import React, {Component } from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import LoginScreen from './screens/LoginScreen';
import HomeScreen from './screens/HomeScreen';
import CharactersScreen from './screens/CharactersScreen';
import CharDetailScreen from './screens/CharDetailScreen';
import QuotesScreen from './screens/QuotesScreen';

const Stack = createStackNavigator();

export default class App extends Component {
  render(){
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="LoginScreen" component={LoginScreen} options={{headerShown:false}}/>
          <Stack.Screen name="HomeScreen" component={HomeScreen} options={{headerShown:false}}/>
          <Stack.Screen name="CharactersScreen" component={CharactersScreen} options={{headerShown:false}}/>
          <Stack.Screen 
            name="CharDetailScreen" 
            component={CharDetailScreen}
            options={{ 
              title: 'Detail',
              headerStyle: {
                backgroundColor: '#222',
              },
              headerTintColor: '#fff',
			    A
              headerTitleStyle: {
                fontWeight: 'bold',
              }, 
            }}
          />
          <Stack.Screen name="QuotesScreen" component={QuotesScreen} options={{headerShown:false}}/>
        </Stack.Navigator>
      </NavigationContainer>
    );
  }  
}
