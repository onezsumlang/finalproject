import React, { Component } from 'react';
import { 
  StyleSheet, Text, View, Image,
  Dimensions, TouchableOpacity, ScrollView, ActivityIndicator
} from 'react-native';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import renderIf from '../components/renderif';

const DEVICE = Dimensions.get('window');

export default class LoginScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      hiddenViewChar:true,
      data: [{
        "char_id": 1,
        "name": "Walter White",
        "birthday": "09-07-1958",
        "occupation": [
          "High School Chemistry Teacher",
          "Meth King Pin"
        ],
        "img": "https://images.amcnetworks.com/amc.com/wp-content/uploads/2015/04/cast_bb_700x1000_walter-white-lg.jpg",
        "status": "Presumed dead",
        "nickname": "Heisenberg",
        "appearance": [
          1,
          2,
          3,
          4,
          5
        ],
        "portrayed": "Bryan Cranston",
        "category": "Breaking Bad",
        "better_call_saul_appearance": []
      }],
      isLoading: true,
      isError: false,
      chooseCount: 4
    }
  }

  componentDidMount(){
    this.toggleViewChar();
  }

  toggleViewChar = async () => {
    // this.setState({
    //   hiddenViewChar:!this.state.hiddenViewChar
    // });
    // console.log('toggle button handler: '+ this.state.hiddenViewChar);
    try{
      const response = await Axios.get('https://breakingbadapi.com/api/character/random');
      this.setState({ hiddenViewChar: false, isError: false, isLoading: false, data: response.data, chooseCount: this.state.chooseCount-1 });
    }catch(error){
      this.setState({ hiddenViewChar: true, isLoading: false, isError: true });
      console.log(error);
    }
    // console.log(this.state)
  }

  render(){
    if(this.state.isLoading){
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
          <ActivityIndicator size='large' color='#055338' />
        </View>
      );
    }else if(this.state.isError){
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
          <Text>Terjadi Error Saat Memuat Data, silahkan restart ulang!</Text>
        </View>
      );
    }
    console.log(this.state.data);
    // const data = this.state.data[0];
    return (
      <View style={styles.container}>
        <ScrollView style={{ marginBottom: 60 }}>
          <View style={styles.imageHeader}>
            <Image source={require('../assets/logo_white.png')} />
          </View>
          <View style={{ paddingVertical: 10 }}>
            <Text style={styles.text1}>
              "There is gold in the streets just waiting for someone to come and scoop it up."
            </Text>
            <Text style={styles.text1}>
              — Walter White, Season 5, Madrigal
            </Text>
          </View>    
          <View style={{ paddingLeft: 20 }}>
            <Text style={styles.text2}>Hi, Onez Sumlang!</Text>
            <Text style={styles.text2}>Choose your character wisely!</Text>
          </View>   
          { !this.state.hiddenViewChar &&
          <View style={{flexDirection: 'row', marginVertical: 25, fontSize: 15, backgroundColor: '#fff', borderTopStartRadius: 20, borderTopEndRadius: 20 }}>
            <Image source={{uri: this.state.data[0].img}} style={{width: 150, height: 150, resizeMode: 'contain'}} />
            <View style={{ justifyContent: 'space-between', padding: 10 }}>
              <Text style={{ color: '#000', fontSize: 18 }}>
                Name : <Text style={{ fontWeight: 'bold', width: 100 }}>{this.state.data[0].name}</Text>{"\n"}
                Nickname : <Text style={{ fontWeight: 'bold', width: 100 }}>{this.state.data[0].nickname}</Text>{"\n"}
                Birthday : {this.state.data[0].birthday ? this.state.data[0].birthday : 'Unknown'}
              </Text>
              <TouchableOpacity 
                style={this.state.chooseCount == 0 ? styles.btnChooseAgainDisabled : styles.btnChooseAgain}
                onPress={()=>this.toggleViewChar()}
                disabled={this.state.chooseCount == 0 ? true : false}>
                <Text style={{ color: '#fff', fontWeight: 'bold' }}>Choose again? ({this.state.chooseCount})</Text>
              </TouchableOpacity>
            </View>
          </View>
          }
        </ScrollView>

        <View style={styles.tabMenuWrap}>
          <TouchableOpacity 
            style={styles.tabItem}
            onPress={() => this.props.navigation.navigate('HomeScreen')}>
            <Icon name="home" size={25} color="#555"/>
            <Text style={styles.tabTitle}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.tabItem} 
            onPress={() => this.props.navigation.navigate('CharactersScreen')}>
            <Icon name="folder-account" size={25} color="#555"/>
            <Text style={styles.tabTitle}>Characters</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.tabItem}
            onPress={() => this.props.navigation.navigate('QuotesScreen')}>
            <Icon name="format-quote-close" size={25} color="#555"/>
            <Text style={styles.tabTitle}>Quotes</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.tabItem}
            onPress={() => this.props.navigation.navigate('LoginScreen')}>
            <Icon name="power" size={25} color="#555"/>
            <Text style={styles.tabTitle}>Sign Out</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
  },
  imageHeader: {
    flexDirection: 'row', 
    justifyContent: 'center', 
    marginVertical: 25
  },
  text1: {
    color: '#fff', fontSize: 20, fontStyle: 'italic', textAlign: 'center'
  },
  text2: {
    color: '#fff', fontSize: 18, fontStyle: 'italic'
  },
  btnChooseAgain: {
    backgroundColor: '#d09c0b', width:150, padding: 10, alignItems: 'center', borderRadius: 5
  },
  btnChooseAgainDisabled: {
    backgroundColor: '#ccc', width:150, padding: 10, alignItems: 'center', borderRadius: 5
  },
  tabMenuWrap: {
    width: '100%', height: 60, 
    backgroundColor: '#000', 
    position: 'absolute', 
    bottom: 0, left: 0,
    borderTopWidth: 1, borderTopColor: '#222',
    flexDirection: 'row',
    justifyContent: 'space-around'
  }, 
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabTitle: {
    fontSize: 14,
    color: '#666',
  },
});