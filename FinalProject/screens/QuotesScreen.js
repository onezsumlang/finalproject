import React, { Component } from 'react';
import {
  TouchableOpacity,
  StyleSheet, FlatList,
  Text, Dimensions, View, ActivityIndicator, Image, ScrollView, SafeAreaView
} from 'react-native';
import Axios from 'axios';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const DEVICE = Dimensions.get('window');

function Item({data}){
  return (
    <View style={{ flexDirection: 'column', marginVertical: 10, padding:15, borderRadius:10, backgroundColor: '#222' }}>
      <Text style={{ color: '#fff', fontSize: 18 }}>"{data.quote}"</Text>
      <Text style={{ color: '#ddd', fontSize: 16, textAlign: 'right', fontStyle: 'italic' }}><Text style={{ fontWeight: 'bold' }}>{data.author}</Text>, {data.series}</Text>
    </View>
  );
}

export default class CharactersScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false
    };
  }

  componentDidMount(){
    this.getQuotes();
  }

  getQuotes = async () => {
    try{
      const response = await Axios.get('https://breakingbadapi.com/api/quotes');
      this.setState({ isError: false, isLoading: false, data: response.data });
    }catch(error){
      this.setState({ isLoading: false, isError: true });
    }
  }

  render(){
    if(this.state.isLoading){
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
          <ActivityIndicator size='large' color='#055338' />
        </View>
      );
    }else if(this.state.isError){
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      );
    }

    console.log(this.state.data);
    const renderItem = ({ item }) => (
      <Item data={item} />
    );

    return (
      <View style={styles.container}>
        <SafeAreaView style={{ flexDirection: 'row', marginTop: 25, marginBottom: 60 }}>
          <FlatList
            data={this.state.data}
            renderItem={renderItem}
            keyExtractor={item => item.quote_id}
          />
        </SafeAreaView>

        <View style={styles.tabMenuWrap}>
          <TouchableOpacity 
            style={styles.tabItem}
            onPress={() => this.props.navigation.navigate('HomeScreen')}>
            <Icon name="home" size={25} color="#555"/>
            <Text style={styles.tabTitle}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.tabItem} 
            onPress={() => this.props.navigation.navigate('CharactersScreen')}>
            <Icon name="folder-account" size={25} color="#555"/>
            <Text style={styles.tabTitle}>Characters</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.tabItem}
            onPress={() => this.props.navigation.navigate('QuotesScreen')}>
            <Icon name="format-quote-close" size={25} color="#555"/>
            <Text style={styles.tabTitle}>Quotes</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.tabItem}
            onPress={() => this.props.navigation.navigate('LoginScreen')}>
            <Icon name="power" size={25} color="#555"/>
            <Text style={styles.tabTitle}>Sign Out</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
  },
  tabMenuWrap: {
    width: '100%', height: 60, 
    backgroundColor: '#000', 
    position: 'absolute', 
    bottom: 0, left: 0,
    borderTopWidth: 1, borderTopColor: '#222',
    flexDirection: 'row',
    justifyContent: 'space-around'
  }, 
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabTitle: {
    fontSize: 14,
    color: '#666',
  },
});