import React, { Component } from 'react';
import {
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  Text, Dimensions, View, TextInput, Image, ActivityIndicator
} from 'react-native';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const DEVICE = Dimensions.get('window');

function Item({ data, onPress }) {
  return (
    <View style={{flexDirection: 'row', fontSize: 15, backgroundColor: '#222', borderRadius: 20, marginBottom: 10 }}>
      <Image source={{uri: data.img}} style={{width: 150, height: 150, resizeMode: 'contain'}} />
      <View style={{ justifyContent: 'space-between', padding: 10 }}>
        <Text style={{ color: '#ccc', fontSize: 18 }}>
          Name : <Text style={{ fontWeight: 'bold' }}>{data.name}</Text>{"\n"}
          Nickname : <Text style={{ fontWeight: 'bold' }}>{data.nickname}</Text>{"\n"}
          Status : {data.status}
        </Text>
        <TouchableOpacity 
          style={{ width: 100, backgroundColor: '#055338', padding: 10, alignItems: 'center', borderRadius: 5 }}
          onPress={onPress}
          >
          <Text style={{ color: '#fff', fontWeight: 'bold' }}>View</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default class CharactersScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false
    };
  }

  componentDidMount(){
    this.getDetailChar();
  }

  getDetailChar = async () => {
    try{
      const response = await Axios.get('https://breakingbadapi.com/api/characters');
      this.setState({ isError: false, isLoading: false, data: response.data });
    }catch(error){
      this.setState({ isLoading: false, isError: true });
    }
  }

  render(){
    if(this.state.isLoading){
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
          <ActivityIndicator size='large' color='#055338' />
        </View>
      );
    }else if(this.state.isError){
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      );
    }

    const renderItem = ({ item }) => (
      <Item data={item} onPress={() => this.props.navigation.navigate('CharDetailScreen', { char_id: item.char_id})} />
    );
    
    return (
      <View style={styles.container}>

        <View style={styles.searchSection}>
          <Icon style={styles.searchIcon} name="account-search" size={20} color="#055338"/>
          <TextInput
            style={styles.input}
            placeholder="Search character..."
            underlineColorAndroid="transparent"
          />
        </View>

        <SafeAreaView style={{ flexDirection: 'row', marginBottom: 150 }}>
          <FlatList
            data={this.state.data}
            renderItem={renderItem}
            keyExtractor={item => item.char_id}
          />
        </SafeAreaView>

        <View style={styles.tabMenuWrap}>
          <TouchableOpacity 
            style={styles.tabItem}
            onPress={() => this.props.navigation.navigate('HomeScreen')}>
            <Icon name="home" size={25} color="#555"/>
            <Text style={styles.tabTitle}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.tabItem} 
            onPress={() => this.props.navigation.navigate('CharactersScreen')}>
            <Icon name="folder-account" size={25} color="#555"/>
            <Text style={styles.tabTitle}>Characters</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.tabItem}
            onPress={() => this.props.navigation.navigate('QuotesScreen')}>
            <Icon name="format-quote-close" size={25} color="#555"/>
            <Text style={styles.tabTitle}>Quotes</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.tabItem}
            onPress={() => this.props.navigation.navigate('LoginScreen')}>
            <Icon name="power" size={25} color="#555"/>
            <Text style={styles.tabTitle}>Sign Out</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
  },
  searchSection: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: '#fff',
    marginTop: 25, marginBottom: 10,
    borderWidth: 1, width: DEVICE.width*0.98,
    borderColor: '#ccc', height: 60
  },
  searchIcon: {
    padding: 10,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: '#fff', borderTopWidth: 1, borderBottomWidth: 1, borderColor: '#ccc',
    color: '#424242', height: 60,
  },
  tabMenuWrap: {
    width: '100%', height: 60, 
    backgroundColor: '#000', 
    position: 'absolute', 
    bottom: 0, left: 0,
    borderTopWidth: 1, borderTopColor: '#222',
    flexDirection: 'row',
    justifyContent: 'space-around'
  }, 
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabTitle: {
    fontSize: 14,
    color: '#666',
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginBottom: 10,
    marginHorizontal: 10,
  },
  title: {
    fontSize: 32,
  },
});