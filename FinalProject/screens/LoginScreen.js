import React, { Component } from 'react';
import { 
  StyleSheet, Text, View, Image,
  Dimensions, TextInput, TouchableOpacity
} from 'react-native';

const DEVICE = Dimensions.get('window');

function checkLogin(){

}

export default class LoginScreen extends Component {
  constructor(props){
    super(props);
    this.state = {
      username: '',
      password: '',
      isError: false,
    };
  }

  loginHandler() {
    console.log(this.state.username, ' ', this.state.password);
    if(this.state.username == 'onez' && this.state.password == '1234'){
      this.props.navigation.navigate('HomeScreen', {username: this.state.username});
    }else{
      this.setState({isError:true});
    }
    console.log(this.state);
  }

  render(){    
    return (
      <View style={styles.container}>
        <View style={styles.loginHeader}>
          <Image source={require('../assets/login_header.png')} style={styles.loginHeaderImage}/>
        </View>
        <View style={styles.loginBody}>
          <Text style={styles.textBodyTop}>"La Familia Es Todo"</Text>
          <View style={styles.textInputWrap}>
            <TextInput
              style={styles.textInput}
              placeholder='username or email'
              placeholderTextColor='#055338'
              value={this.state.username}
              onChangeText={username => this.setState({ username })}
            />
          </View>
          <View style={styles.textInputWrap}>
            <TextInput
              style={styles.textInput} 
              placeholder='password'
              placeholderTextColor='#055338'
              onChangeText={password => this.setState({ password })}
              secureTextEntry={true}
            />
          </View>
          <TouchableOpacity 
            style={styles.btnLogin} 
            onPress={() => this.loginHandler()}>
            <Text style={styles.btnText}>Sign In</Text>
          </TouchableOpacity>
          <Text style={styles.textBodyBottom}>Don't have an account? <Text style={{fontWeight: 'bold'}}>Sign Up</Text></Text>
          <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Username/ Password Salah</Text>
        </View>
      </View>
    );
  }  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  loginHeader: {
    
  },
  loginHeaderImage: {
    width: DEVICE.width
  },
  loginBody: {
    flexDirection: 'column',
    alignItems: 'center'
  },
  textBodyTop: {
    fontSize: 24, fontStyle: 'italic', 
    marginVertical: 25, color: '#055338'
  },
  textBodyBottom: {
    color: '#055338'
  },
  textInputWrap: {
    marginBottom: 15
  },
  textInput: {
    width: DEVICE.width*0.88, 
    height: 50,
    borderColor: '#ccc', 
    borderWidth: 1, paddingLeft: 10, fontSize: 18,
    color: '#055338'
  },
  btnLogin: {
    width: DEVICE.width*0.88,
    marginBottom: 5,
    backgroundColor: '#055338',
    alignItems: 'center',
    padding: 10
  },
  btnText: {
    color: '#fff',
    fontSize: 18, fontWeight: 'bold'
  },
  errorText: {
    color: 'red',
    textAlign: 'center',
    marginTop: 16,
  },
  hiddenErrorText: {
    color: 'transparent',
    textAlign: 'center',
    marginTop: 16,
  }
});
