import React, { Component } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Text, Dimensions, View, ActivityIndicator, Image, ScrollView
} from 'react-native';
import Axios from 'axios';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const DEVICE = Dimensions.get('window');

export default class CharDetailScreen extends Component {
  constructor(props){
    super(props);
    const { route, navigation } = this.props;

    this.state = {
      data: {},
      isLoading: true,
      isError: false,
      charId: route.params.char_id
    };
  }

  componentDidMount(){
    this.getDetailChar();
  }

  getDetailChar = async () => {
    try{
      const response = await Axios.get('https://breakingbadapi.com/api/characters/'+this.state.charId);
      this.setState({ isError: false, isLoading: false, data: response.data });
    }catch(error){
      this.setState({ isLoading: false, isError: true });
    }
  }

  render(){
    if(this.state.isLoading){
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
          <ActivityIndicator size='large' color='#055338' />
        </View>
      );
    }else if(this.state.isError){
      return (
        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      );
    }
    // console.log(this.props.route.char_id);
    // const { route, navigation } = this.props;
    // console.log(this.state.charId);
    const data = this.state.data[0];
    return (
      <View style={styles.container}>
        <ScrollView style={{marginBottom: 30}}>
          <View style={{ marginVertical: 30, flexDirection: 'column', alignItems: 'center' }}>
            <Image source={{ uri: data.img }} style={{width: 200, height: 200, resizeMode: 'contain'}} />
            <View style={{ marginTop: 10, alignItems: 'center' }}>
              <View style={{ width: DEVICE.width*0.9, flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={{ fontSize: 18, color: '#666' }}>Name</Text>
                <Text style={{ fontSize: 18, color: '#fff' }}>{data.name}</Text>
              </View>
              <View style={{ width: DEVICE.width*0.9, flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={{ fontSize: 18, color: '#666' }}>Nickname</Text>
                <Text style={{ fontSize: 18, color: '#fff' }}>{data.nickname}</Text>
              </View>
              <View style={{ width: DEVICE.width*0.9, flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={{ fontSize: 18, color: '#666' }}>Birthday</Text>
                <Text style={{ fontSize: 18, color: '#fff' }}>{data.birthday}</Text>
              </View>
              <View style={{ width: DEVICE.width*0.9, flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={{ fontSize: 18, color: '#666' }}>Occupation</Text>              
                <View>
                  {data.occupation.map(function(name, index){
                    return <Text style={{ fontSize: 18, color:'#fff' }} key={ index }>- {name}</Text>;
                  })}
                </View>
              </View>
              <View style={{ width: DEVICE.width*0.9, flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={{ fontSize: 18, color: '#666' }}>Appearance</Text>
                <View>
                  {data.appearance.map(function(name, index){
                    return <Text style={{ fontSize: 18, color:'#fff' }} key={ index }>Session {name}</Text>;
                  })}
                </View>
              </View>
              <View style={{ width: DEVICE.width*0.9, flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text style={{ fontSize: 18, color: '#666' }}>Status</Text>
                <Text style={{ fontSize: 18, color: '#fff' }}>{data.status}</Text>
              </View>
            </View>
          </View>
          
          {/* <View style={{ marginVertical: 10, alignItems: 'center' }}>
            <Text style={{ fontSize: 18, fontStyle: 'italic', color: '#fff' }}>"How much is enough?" - Walter White</Text>
          </View> */}
        </ScrollView>

        <View style={styles.tabMenuWrap}>
        <TouchableOpacity 
            style={styles.tabItem}
            onPress={() => this.props.navigation.navigate('HomeScreen')}>
            <Icon name="home" size={25} color="#555"/>
            <Text style={styles.tabTitle}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.tabItem} 
            onPress={() => this.props.navigation.navigate('CharactersScreen')}>
            <Icon name="folder-account" size={25} color="#555"/>
            <Text style={styles.tabTitle}>Characters</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.tabItem}
            onPress={() => this.props.navigation.navigate('QuotesScreen')}>
            <Icon name="format-quote-close" size={25} color="#555"/>
            <Text style={styles.tabTitle}>Quotes</Text>
          </TouchableOpacity>
          <TouchableOpacity 
            style={styles.tabItem}
            onPress={() => this.props.navigation.navigate('LoginScreen')}>
            <Icon name="power" size={25} color="#555"/>
            <Text style={styles.tabTitle}>Sign Out</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
  },
  tabMenuWrap: {
    width: '100%', height: 60, 
    backgroundColor: '#000', 
    position: 'absolute', 
    bottom: 0, left: 0,
    borderTopWidth: 1, borderTopColor: '#222',
    flexDirection: 'row',
    justifyContent: 'space-around'
  }, 
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  tabTitle: {
    fontSize: 14,
    color: '#666',
  },
});